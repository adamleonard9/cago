// +build mage

package main

import (
	"errors"
	"fmt"
	"os"
	"strings"

	"github.com/magefile/mage/mg"
	"github.com/magefile/mage/sh"

	"github.com/xanzy/go-gitlab"
)

const (
	CCOS   = "windows darwin linux"
	CCARCH = "amd64"

	BUILD_DIR   = "build"
	RELEASE_DIR = "release"
	BINARY_NAME = "cago"

	WINDOWS_SCRIPT = "cago.bat"
	NIX_SCRIPT     = "cago.sh"

	SCRIPT_DIR = "scripts"
)

func Banner() error {
	workingDir, _ := sh.Output("pwd")
	goVersion, _ := sh.Output("go", "version")
	goPath, _ := sh.Output("go", "env", "GOPATH")
	goRoot, _ := sh.Output("go", "env", "GOROOT")

	banner := "===================================================================================\n" +
		"Working dir:    " + workingDir + "\n" +
		"Go version:     " + goVersion + "\n" +
		"Go path:        " + goPath + "\n" +
		"Go root:        " + goRoot + "\n" +
		"==================================================================================="

	fmt.Println(banner)

	return nil
}

func Clean() error {
	mg.Deps(Banner)

	fmt.Println("==> Cleaning")

	buildDirError := sh.Rm(BUILD_DIR)
	if buildDirError != nil {
		return buildDirError
	}

	releaseDirError := sh.Rm(RELEASE_DIR)
	if releaseDirError != nil {
		return releaseDirError
	}

	fmt.Println("==> Finished cleaning")

	return nil
}

func Build() error {
	mg.Deps(Banner)

	fmt.Println("==> Running go build")
	buildError := sh.RunV("go", "build", "./...")
	if buildError != nil {
		return buildError
	}
	fmt.Println("==> Finished go build")

	return nil
}

func Lint() error {
	mg.Deps(Banner, Build)

	fmt.Println("==> Running go vet")
	vetError := sh.RunV("go", "vet", "./...")
	if vetError != nil {
		return vetError
	}
	fmt.Println("==> Finished go vet")

	fmt.Println("==> Running golangci-lint")
	lintError := sh.RunV("golangci-lint", "run", "./...")
	if lintError != nil {
		return lintError
	}
	fmt.Println("==> Finished golangci-lint")

	return nil
}

func Test() error {
	mg.Deps(Banner, Build, Lint)

	fmt.Println("==> Running go test")
	testError := sh.RunV("go", "test", "-coverprofile=profile.out", "-covermode=atomic", "./...")
	if testError != nil {
		return testError
	}
	fmt.Println("==> Finished go test")

	return nil
}

func CrossCompile() error {
	mg.Deps(Banner, Clean, Test, Build)

	os.MkdirAll(BUILD_DIR, os.ModePerm)

	cagoVersion, cagoVersionError := sh.Output("git", "describe", "--tags")
	if cagoVersionError != nil {
		return cagoVersionError
	}

	fmt.Println("==> Running Gox to cross-compile Cago version: " + cagoVersion)
	goxError := sh.RunV("gox",
		"-ldflags=-X electric-it.io/cago/cmd.Version="+cagoVersion,
		"-os="+CCOS,
		"-arch="+CCARCH,
		"-output="+BUILD_DIR+"/{{.OS}}-{{.Arch}}/"+BINARY_NAME,
	)
	if goxError != nil {
		return goxError
	}
	fmt.Println("==> Finished running Gox")

	return nil
}

func Package() error {
	mg.Deps(Banner, Clean, CrossCompile)

	cagoVersion, cagoVersionError := sh.Output("git", "describe", "--tags")
	if cagoVersionError != nil {
		return cagoVersionError
	}

	fmt.Println("==> Packaging Cago version: " + cagoVersion)

	os.MkdirAll(RELEASE_DIR, os.ModePerm)

	osList := strings.Fields(CCOS)
	archList := strings.Fields(CCARCH)

	for _, os := range osList {
		for _, arch := range archList {
			packageDir := BUILD_DIR + "/" + os + "-" + arch + "/"
			fmt.Println("Packaging " + packageDir)

			if os == "windows" {
				zipFile := RELEASE_DIR + "/cago-" + os + "-" + arch + "-" + cagoVersion + ".zip"

				sh.Rm(zipFile)

				zipError := sh.RunV("zip", "-j", zipFile, packageDir+"cago.exe", "scripts/"+WINDOWS_SCRIPT)
				if zipError != nil {
					return zipError
				}
			} else {
				zippedTarFile := RELEASE_DIR + "/cago-" + os + "-" + arch + "-" + cagoVersion + ".tar.gz"

				sh.Rm(zippedTarFile)

				tarError := sh.RunV("tar", "-czf", zippedTarFile, "-C", packageDir, "cago", "-C", "../../"+SCRIPT_DIR, NIX_SCRIPT)
				if tarError != nil {
					return tarError
				}
			}
		}
	}

	fmt.Println("==> Finished packaging")

	return nil
}

func GitLabRelease() error {
	mg.Deps(Banner, Clean, Package)

	apiToken := os.Getenv("GITLAB_API_TOKEN")
	if apiToken == "" {
		return errors.New("API not found")
	}

	projectID := os.Getenv("CI_PROJECT_ID")
	if projectID == "" {
		return errors.New("Project ID not found")
	}

	commitTag := os.Getenv("CI_COMMIT_TAG")
	if commitTag == "" {
		return errors.New("Commit tag not found")
	}

	jobID := os.Getenv("CI_JOB_ID")
	if jobID == "" {
		return errors.New("Job ID not found")
	}

	fmt.Println("==> Releasing Cago version: " + commitTag)

	git := gitlab.NewClient(nil, apiToken)

	releaseDescription := fmt.Sprintf("This is Cago release %s", commitTag)

	releaseOptions := &gitlab.CreateReleaseOptions{
		Name:        &commitTag,
		TagName:     &commitTag,
		Description: &releaseDescription,
		Assets: &gitlab.ReleaseAssets{
			Links: []*gitlab.ReleaseAssetLink{
				{
					Name: "MacOS",
					URL:  "https://gitlab.com/electric-it/cago/-/jobs/" + jobID + "/artifacts/raw/release/cago-darwin-amd64-" + commitTag + ".tar.gz",
				},
				{
					Name: "Linux",
					URL:  "https://gitlab.com/electric-it/cago/-/jobs/" + jobID + "/artifacts/raw/release/cago-linux-amd64-" + commitTag + ".tar.gz",
				},
				{
					Name: "Windows",
					URL:  "https://gitlab.com/electric-it/cago/-/jobs/" + jobID + "/artifacts/raw/release/cago-windows-amd64-" + commitTag + ".zip",
				},
			},
		},
	}

	_, _, releaseError := git.Releases.CreateRelease(projectID, releaseOptions)
	if releaseError != nil {
		fmt.Printf("Failed to release: %s\n", releaseError)

		return releaseError
	}

	fmt.Println("==> Finishing releasing")

	return nil
}
