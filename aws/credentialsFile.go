package aws // import "electric-it.io/cago/aws"
import (
	"os"
	"path/filepath"
	"runtime"
	"sync"

	"github.com/apex/log"
	"github.com/go-ini/ini"
	"github.com/mitchellh/go-homedir"
	"github.com/pkg/errors"
	"github.com/spf13/viper"
)

var (
	credentialsFile *ini.File

	credentialsFileLoadError error

	CredentialsFileLoadSync sync.Once
)

func getAWSCredentialsFile() (*ini.File, error) {
	CredentialsFileLoadSync.Do(loadAWSCredentialsFile)

	return credentialsFile, credentialsFileLoadError
}

// GetCredentialsFile Returns the credentials file, creates a new empty file if it does not already exist
func loadAWSCredentialsFile() {
	if viper.GetBool("TESTMODE") {
		log.Debug("Using mock credentials file")
		credentialsFile = ini.Empty()
		return
	}

	// Check to see if a credential file already exists
	if _, statError := os.Stat(CredentialsFilePath); statError == nil {
		// If so, load it
		log.Debugf("Loading an existing credentials file: %s", CredentialsFilePath)
		credentialsFile, credentialsFileLoadError = ini.Load(CredentialsFilePath)
		if credentialsFileLoadError != nil {
			credentialsFileLoadError = errors.Wrapf(credentialsFileLoadError, "Unable to load credentials file: %s", CredentialsFilePath)
		}
	} else {
		// If not, create it
		log.Debugf("Credentials file not found, creating empty file: %s", CredentialsFilePath)
		mkdirError := os.MkdirAll(filepath.Dir(CredentialsFilePath), credentialsFileDirMode)
		if mkdirError != nil {
			log.Errorf("Unable to create directory: %s", CredentialsFilePath)

			credentialsFileLoadError = mkdirError
		}

		log.Debugf("Creating new credentials file: %s", CredentialsFilePath)
		rawCredentialsFile, createError := os.Create(CredentialsFilePath)
		if createError != nil {
			log.Errorf("Unable to create empty credentials file: %s", CredentialsFilePath)

			credentialsFileLoadError = createError
		}

		if runtime.GOOS != "windows" {
			log.Debugf("Updating credentials file mode")
			chmodError := rawCredentialsFile.Chmod(credentialsFileMode)
			if chmodError != nil {
				log.Errorf("Unable to chmod credentials file: %s", CredentialsFilePath)

				credentialsFileLoadError = chmodError
			}
		}

		log.Debugf("Saving empty credentials file: %s", CredentialsFilePath)
		credentialsFile = ini.Empty()
		saveError := credentialsFile.SaveTo(CredentialsFilePath)
		if saveError != nil {
			log.Errorf("Unable to save credentials file: %s", CredentialsFilePath)

			credentialsFileLoadError = saveError
		}
	}
}

func getCredentialsFilePath() string {
	homeDirPath, homedirError := homedir.Dir()
	if homedirError != nil {
		log.Fatalf("Unable to open user's home directory, bailing out: %+v", homedirError)
	}

	return homeDirPath + string(os.PathSeparator) + ".aws" + string(os.PathSeparator) + "credentials"
}
