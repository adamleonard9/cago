// Copyright (c) 2015, Remind101, Inc.
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice, this
//    list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

// Source https://github.com/remind101/empire/blob/master/pkg/arn/arn.go

// Package aws is a Go package for parsing Amazon Resource Names.
package aws // import "electric-it.io/cago/aws"

import (
	"strings"

	"github.com/go-errors/errors"
)

var (
	// ErrInvalidARN may be returned when parsing a string that is not a valid ARN.
	ErrInvalidARN = errors.New("invalid ARN")

	// ErrInvalidARNBadPrefix may be returned when parsing a string that is not a valid ARN.
	ErrInvalidARNBadPrefix = errors.New("ARN must begin with `arn`")

	// ErrInvalidResource may be returned when an ARN Resource is not valid.
	ErrInvalidResource = errors.New("invalid ARN resource")
)

const delimiter = ":"

// ARN represents a parsed Amazon Resource Name.
type ARN struct {
	ARN       string
	Partition string
	Service   string
	Region    string
	Account   string
	Resource  string
}

// ParseARN parses an Amazon Resource Name from a String into an ARN.
func ParseARN(arn string) (*ARN, error) {
	p := strings.SplitN(arn, delimiter, 6)

	// Ensure that we have all the components that make up an ARN.
	if len(p) < 6 {
		return nil, ErrInvalidARN
	}

	a := &ARN{
		ARN:       p[0],
		Partition: p[1],
		Service:   p[2],
		Region:    p[3],
		Account:   p[4],
		Resource:  p[5],
	}

	// ARNs always start with "arn" (hopefully).
	if a.ARN != "arn" {
		return nil, ErrInvalidARNBadPrefix
	}

	return a, nil
}

// String returns the string representation of an Amazon Resource Name.
func (a *ARN) String() string {
	return strings.Join(
		[]string{a.ARN, a.Partition, a.Service, a.Region, a.Account, a.Resource},
		delimiter,
	)
}
