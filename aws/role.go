package aws // import "electric-it.io/cago/aws"

// Original Source: https://github.com/Versent/saml2aws
// Original License:
//   This code is Copyright (c) 2015 Versent and released under the MIT license.
//   All rights not explicitly granted in the MIT license are reserved.
//

import (
	"fmt"
	"strings"

	"github.com/apex/log"
	"github.com/go-errors/errors"
)

// Role aws role attributes
type Role struct {
	RoleARN      string
	PrincipalARN string
}

// ParseAWSRoles parses and splits the roles while also validating the contents
func ParseAWSRoles(roles []string) ([]*Role, error) {
	log.Debugf("Parsing %d roles", len(roles))

	awsRoles := make([]*Role, len(roles))

	for i, role := range roles {
		awsRole, parseRoleError := parseRole(role)
		if parseRoleError != nil {
			return nil, parseRoleError
		}

		awsRoles[i] = awsRole
	}

	return awsRoles, nil
}

func parseRole(role string) (*Role, error) {
	tokens := strings.Split(role, ",")

	if len(tokens) != 2 {
		return nil, errors.Errorf("Invalid role string only %d tokens", len(tokens))
	}

	awsRole := &Role{}

	for _, token := range tokens {
		if strings.Contains(token, ":saml-provider") {
			awsRole.PrincipalARN = token
		}
		if strings.Contains(token, ":role") {
			awsRole.RoleARN = token
		}
	}

	if awsRole.PrincipalARN == "" {
		return nil, fmt.Errorf("Unable to locate PrincipalARN in: %v", role)
	}

	if awsRole.RoleARN == "" {
		return nil, fmt.Errorf("Unable to locate RoleARN in: %v", role)
	}

	return awsRole, nil
}
