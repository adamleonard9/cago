package aws // import "electric-it.io/cago/aws"

import (
	"os"
	"sort"
	"time"

	"github.com/apex/log"
	"github.com/pkg/errors"
	"github.com/spf13/viper"
)

const (
	// Permissions for the credentials file directory
	credentialsFileDirMode os.FileMode = 0700

	// Permissions for the credentials file
	credentialsFileMode os.FileMode = 0600

	// ExpirationKeyName is the key in the credentials file indicating when the profile will expire
	ExpirationKeyName = "expire"

	// CagoManagedKeyName it the key in the credentials file indicating the profile is managed by Cago
	CagoManagedKeyName = "cago_managed"
)

var (
	// CredentialsFilePath is the path to the AWS credentials file; Defaults to ~/.aws/credentials
	CredentialsFilePath = getCredentialsFilePath()
)

// SetExpiration sets the expiration time for the profile
func SetExpiration(profileName string, expiration time.Time) error {
	// Grab the credentials file
	credentialsFile, getCredentialsFileError := getAWSCredentialsFile()
	if getCredentialsFileError != nil {
		return errors.Wrapf(getCredentialsFileError, "Unable to set expiration for profile: %s", profileName)
	}

	// Grab the section
	profileSection, getSectionError := credentialsFile.GetSection(profileName)
	if getSectionError != nil {
		return errors.Wrapf(getSectionError, "Cannot set expiration, profile does not exist: %s", profileName)
	}

	if !profileSection.HasKey(CagoManagedKeyName) {
		return errors.Errorf("Profile is not being managed by Cago: %s", profileName)
	}

	_, newKeyError := profileSection.NewKey(ExpirationKeyName, expiration.Format(time.RFC3339))
	if newKeyError != nil {
		return errors.Wrapf(newKeyError, "Unable to set expiration in credentials file: %s", profileName)
	}

	if viper.GetBool("TESTMODE") {
		log.Debug("Test mode enabled, skipping save")
		return nil
	}

	log.Debugf("Saving credentials file to: %s", CredentialsFilePath)

	saveError := credentialsFile.SaveTo(CredentialsFilePath)
	if saveError != nil {
		return errors.Wrapf(saveError, "Unable to save credentials file changes to profile: %s", profileName)
	}

	return nil
}

// DoesProfileExist returns true if the profile exists; false if not; error if there was a problem accessing the credentials file
func DoesProfileExist(profileName string) (bool, error) {
	// Grab the credentials file
	credentialsFile, getCredentialsFileError := getAWSCredentialsFile()
	if getCredentialsFileError != nil {
		return false, errors.Wrapf(getCredentialsFileError, "Unable to get expiration for profile: %s", profileName)
	}

	// Grab the section
	_, getSectionError := credentialsFile.GetSection(profileName)
	if getSectionError != nil {
		return false, nil
	}

	return true, nil
}

// IsExpired returns true if the profile has expired or if an error occurs; false if the profile has not expired
func IsExpired(profileName string) (bool, error) {
	// Grab the credentials file
	credentialsFile, getCredentialsFileError := getAWSCredentialsFile()
	if getCredentialsFileError != nil {
		return true, errors.Wrap(getCredentialsFileError, "Unable to open credentials file")
	}

	// Grab the section
	profileSection, getSectionError := credentialsFile.GetSection(profileName)
	if getSectionError != nil {
		return true, errors.Wrapf(getSectionError, "Unable to get section: %s", profileName)
	}

	expirationKey, getKeyError := profileSection.GetKey(ExpirationKeyName)
	if getKeyError != nil {
		return true, errors.Wrapf(getKeyError, "Unable to get expiration key for profile: %s", profileName)
	}

	expirationTime, timeError := expirationKey.Time()
	if timeError != nil {
		return true, errors.Wrapf(timeError, "Unable to parse expiration for profile: %s", profileName)
	}

	if time.Now().After(expirationTime) {
		// The time right now is after the expiration time
		return true, nil
	}

	return false, nil
}

// AddProfile creates a new profile; returns an error if the profile couldn't be created
func AddProfile(profileName string) error {
	// Grab the credentials file
	credentialsFile, getCredentialsFileError := getAWSCredentialsFile()
	if getCredentialsFileError != nil {
		return errors.Wrapf(getCredentialsFileError, "Unable to add profile: %s", profileName)
	}

	// Make sure the section does not already exist
	_, getSectionError := credentialsFile.GetSection(profileName)
	if getSectionError == nil {
		return errors.Errorf("Profile %s already exists", profileName)
	}

	// Create the new section to store the profile
	newSection, newSectionError := credentialsFile.NewSection(profileName)
	if newSectionError != nil {
		return errors.Wrapf(newSectionError, "Unable to add profile: %s", profileName)
	}

	// Mark this new section as being managed by Cago
	_, newKeyError := newSection.NewKey(CagoManagedKeyName, "true")
	if newKeyError != nil {
		return errors.Wrapf(newKeyError, "Unable to create new key for profile: %s", profileName)
	}

	if viper.GetBool("TESTMODE") {
		log.Debug("Test mode enabled, skipping save")
		return nil
	}

	log.Debugf("Saving credentials file to: %s", CredentialsFilePath)

	saveError := credentialsFile.SaveTo(CredentialsFilePath)
	if saveError != nil {
		return errors.Wrapf(saveError, "Unable to save credentials file changes to profile: %s", profileName)
	}

	return nil
}

// DeleteProfile deletes a profile; returns an error if the profile couldn't be deleted
func DeleteProfile(profileName string) error {
	// Grab the credentials file
	credentialsFile, getCredentialsFileError := getAWSCredentialsFile()
	if getCredentialsFileError != nil {
		return errors.Wrapf(getCredentialsFileError, "Unable to delete profile: %s", profileName)
	}

	credentialsFile.DeleteSection(profileName)

	if viper.GetBool("TESTMODE") {
		log.Debug("Test mode enabled, skipping save")
		return nil
	}

	log.Debugf("Saving credentials file to: %s", CredentialsFilePath)

	saveError := credentialsFile.SaveTo(CredentialsFilePath)
	if saveError != nil {
		return errors.Wrapf(saveError, "Unable to save credentials file changes to profile: %s", profileName)
	}

	return nil
}

// AddKeyValue adds a key value pair to the profile
func AddKeyValue(profileName string, keyName string, keyValue string) error {
	credentialsFile, getCredentialsFileError := getAWSCredentialsFile()
	if getCredentialsFileError != nil {
		return errors.Wrap(getCredentialsFileError, "Unable to retrieve credentials file")
	}

	profileSection, getSectionError := credentialsFile.GetSection(profileName)
	if getSectionError != nil {
		return errors.Wrapf(getSectionError, "Unable to retrieve section from credentials file: %s", profileName)
	}

	_, newKeyError := profileSection.NewKey(keyName, keyValue)
	if newKeyError != nil {
		return errors.Wrapf(newKeyError, "Unable to create key in section: %s", profileName)
	}

	if viper.GetBool("TESTMODE") {
		log.Debug("Test mode enabled, skipping save")
		return nil
	}

	log.Debugf("Saving credentials file to: %s", CredentialsFilePath)

	saveError := credentialsFile.SaveTo(CredentialsFilePath)
	if saveError != nil {
		return errors.Wrapf(saveError, "Unable to save credentials file changes to profile: %s", profileName)
	}

	return nil
}

// GetKeyValue returns the value associated with a key for the profile; returns empty string if value does not exist
func GetKeyValue(profileName string, key string) (string, error) {
	credentialsFile, getCredentialsFileError := getAWSCredentialsFile()
	if getCredentialsFileError != nil {
		return "", errors.Wrapf(getCredentialsFileError, "Unable to retrieve credentials file")
	}

	log.Debugf("Getting section: %s", profileName)
	profileSection, getSectionError := credentialsFile.GetSection(profileName)
	if getSectionError != nil {
		return "", errors.Wrapf(getSectionError, "Unable to retrieve section from credentials file: %s", profileName)
	}

	log.Debugf("Getting key: %s", key)
	sectionKey, getKeyError := profileSection.GetKey(key)
	if getKeyError != nil {
		return "", errors.Wrapf(getKeyError, "Unable to retrieve key from section: %s", profileName)
	}

	return sectionKey.Value(), nil
}

// GetAllManagedProfileNames returns a list of the names of all managed profiles
func GetAllManagedProfileNames() ([]string, error) {
	credentialsFile, getCredentialsFileError := getAWSCredentialsFile()
	if getCredentialsFileError != nil {
		return nil, errors.Wrap(getCredentialsFileError, "Unable to retrieve credentials file")
	}

	log.Debug("Reading existing profiles from credentials file")

	// Grab a list of all the sections in the credentials file
	allProfiles := credentialsFile.Sections()
	var profileNames []string
	for _, profile := range allProfiles {
		// Check to see if this is a profile that we're managing
		if profile.HasKey(CagoManagedKeyName) {
			log.Debugf("Profile %s is managed", profile.Name())

			profileNames = append(profileNames, profile.Name())
		} else {
			log.Debugf("Profile %s is not managed, skipping", profile.Name())
		}
	}

	sort.Strings(profileNames)

	return profileNames, nil
}
