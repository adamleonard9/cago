package aws_test // import "electric-it.io/cago/aws"

import (
	"testing"

	"github.com/go-errors/errors"

	"electric-it.io/cago/aws"
)

func TestParseAWSRoles(t *testing.T) {
	{
		roles := []string{}

		parsedRoles, parseAWSRolesError := aws.ParseAWSRoles(roles)
		if parseAWSRolesError != nil {
			t.Fatalf("Unable to parse empty list\n%s", parseAWSRolesError.(*errors.Error).ErrorStack())
		}

		if len(parsedRoles) != 0 {
			t.Errorf("Expected 0; got %d", len(parsedRoles))
		}
	}

	{
		roles := []string{"bad-principal"}

		_, parseAWSRolesError := aws.ParseAWSRoles(roles)
		if parseAWSRolesError == nil {
			t.Fatalf("Expected error when parsing single token")
		}
	}

	{
		roles := []string{"bad-principal,bad-role"}

		_, parseAWSRolesError := aws.ParseAWSRoles(roles)
		if parseAWSRolesError == nil {
			t.Fatalf("Expected error when parsing bad role and principal")
		}
	}

	{
		roles := []string{"bad-role,bad-principal,bad-role,bad-principal"}

		_, parseAWSRolesError := aws.ParseAWSRoles(roles)
		if parseAWSRolesError == nil {
			t.Fatalf("Expected error when parsing multiple pairs with bad role and principal")
		}
	}

	{
		roles := []string{"arn:aws:iam::12345678910:role/role1,bad-principal"}

		_, parseAWSRolesError := aws.ParseAWSRoles(roles)
		if parseAWSRolesError == nil {
			t.Fatalf("Expected error when parsing pair with bad principal")
		}
	}

	{
		roles := []string{"bad-role,arn:aws:iam::12345678910:saml-provider/provider"}

		_, parseAWSRolesError := aws.ParseAWSRoles(roles)
		if parseAWSRolesError == nil {
			t.Fatalf("Expected error when parsing pair with bad role")
		}
	}

	{
		goodRole := "arn:aws:iam::12345678910:role/role1"
		goodPrincipal := "arn:aws:iam::12345678910:saml-provider/provider"

		roles := []string{goodRole + "," + goodPrincipal}

		parsedRoles, parseAWSRolesError := aws.ParseAWSRoles(roles)
		if parseAWSRolesError != nil {
			t.Errorf("Error trying to parse arn:\n%s", parseAWSRolesError.(*errors.Error).ErrorStack())
		}

		if len(parsedRoles) != 1 {
			t.Errorf("Expected 1; got %d", len(parsedRoles))
		}

		if parsedRoles[0].RoleARN != goodRole {
			t.Errorf("Expected %v; got %v", goodRole, parsedRoles[0].RoleARN)
		}

		if parsedRoles[0].PrincipalARN != goodPrincipal {
			t.Errorf("Expected %v; got %v", goodPrincipal, parsedRoles[0].RoleARN)
		}
	}
}
