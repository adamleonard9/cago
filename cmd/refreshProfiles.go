package cmd // import "electric-it.io/cago/cmd"

import (
	"encoding/base64"
	"strings"
	"time"

	"github.com/apex/log"
	awssdk "github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/sts"
	"github.com/aws/aws-sdk-go/service/sts/stsiface"
	"github.com/pkg/errors"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"github.com/thoas/go-funk"

	"gopkg.in/AlecAivazis/survey.v1"

	"electric-it.io/cago/aws"
	"electric-it.io/cago/cmd/credentials"
	"electric-it.io/cago/config"
	"electric-it.io/cago/saml"
)

const (
	forceRefreshFlagLong    = "force-refresh"
	forceRefreshFlagShort   = "f"
	forceRefreshFlagUsage   = "Force a refresh, even if all profiles are valid."
	forceRefreshFlagDefault = false

	configFileFlagLong    = "config-file"
	configFileFlagShort   = "c"
	configFileFlagUsage   = "Path to a local configuration file."
	configFileFlagDefault = ""

	promptForCredentialsFlagLong    = "prompt-for-credentials"
	promptForCredentialsFlagShort   = "p"
	promptForCredentialsFlagUsage   = "Always prompt for credentials, never use the cache."
	promptForCredentialsFlagDefault = false

	sessionDurationFlagLong    = "session-duration"
	sessionDurationFlagShort   = "s"
	sessionDurationFlagUsage   = "The maximum session duration (in seconds) before the roles expire."
	sessionDurationFlagDefault = int64(3600)

	CACHED_USERNAME_MESSAGE = "Using cached username"
	CACHED_PASSWORD_MESSAGE = "Using cached password"
)

var (
	promptForCredentialsFlagValue = promptForCredentialsFlagDefault

	forceRefreshFlagValue = forceRefreshFlagDefault

	configFileFlagValue = configFileFlagDefault

	sessionDurationFlagValue = sessionDurationFlagDefault
)

func init() {
	refreshProfilesCmd.Flags().BoolVarP(&promptForCredentialsFlagValue, promptForCredentialsFlagLong, promptForCredentialsFlagShort, promptForCredentialsFlagDefault, promptForCredentialsFlagUsage)

	refreshProfilesCmd.Flags().BoolVarP(&forceRefreshFlagValue, forceRefreshFlagLong, forceRefreshFlagShort, forceRefreshFlagDefault, forceRefreshFlagUsage)

	refreshProfilesCmd.Flags().StringVarP(&configFileFlagValue, configFileFlagLong, configFileFlagShort, configFileFlagDefault, configFileFlagUsage)

	refreshProfilesCmd.Flags().Int64VarP(&sessionDurationFlagValue, sessionDurationFlagLong, sessionDurationFlagShort, sessionDurationFlagDefault, sessionDurationFlagUsage)

	rootCmd.AddCommand(refreshProfilesCmd)
}

// refreshCmd represents the refresh command
var refreshProfilesCmd = &cobra.Command{
	Use:   "refresh-profiles",
	Short: "Update managed profiles to match authorized roles and refresh all tokens",
	Long:  ``,
	RunE: func(cmd *cobra.Command, args []string) error {
		// ===================================================================
		// Load the configuration file
		loadConfigurationFileError := config.LoadConfigurationFile(configFileFlagValue)
		if loadConfigurationFileError != nil {
			cmd.Printf("Cago could not load a configuration file!\n")
			cmd.Printf("Cago checks the following places in this order:\n")
			cmd.Printf("  1. Local configuration file path specified using command line argument\n")
			cmd.Printf("  2. Remote configuration file URL specified using the CAGO_CONFIG_URL environment variable or the most recently downloaded file\n")
			cmd.Printf("  3. Local configuration file: ~/.cago/cago.yaml\n")

			return loadConfigurationFileError
		}

		// ===================================================================
		// We need a username
		var username string

		// Unless the user wants to force a prompt...
		if !promptForCredentialsFlagValue {
			// ...check the cache.
			username = credentials.GetCachedUsername()
			if username != "" {
				cmd.Println(CACHED_USERNAME_MESSAGE)
			}
		}

		// We may have already gotten the username from the cache...
		if username == "" {
			// ...if not, prompt the user.
			username = promptForUsername()

			if strings.TrimSpace(username) == "" {
				return errors.New("Cago requires a username to proceed")
			}
		}

		// ===================================================================
		// We need a password
		var password string

		// Unless the user wants to force a prompt...
		if !promptForCredentialsFlagValue {
			// ...check the cache.
			password = credentials.GetCachedPassword()
			if password != "" {
				cmd.Println(CACHED_PASSWORD_MESSAGE)
			}
		}

		// We may have already gotten the password from the cache...
		if password == "" {
			// ...if not, prompt the user.
			password = promptForPassword()

			if strings.TrimSpace(password) == "" {
				return errors.New("Cago requires a password to proceed")
			}
		}

		// Cache the username and password for next time 'round
		credentials.CacheUsername(username)
		credentials.CachePassword(password)

		log.Debugf("Calling IdP to retrieve SAML assertion\n")

		// Get a SAML assertion that we can use with the STS
		samlAssertionBase64, samlAssertionErr := saml.GetSAMLAssertionBase64(username, password)
		if samlAssertionErr != nil {
			cmd.Println("Unable to retrieve SAML assertion in response from IdP!")
			cmd.Println("There are several likely causes: ")
			cmd.Println("  1) You entered your username or password incorrectly. Try again, using the '-p' flag to update your cached credentials.")
			cmd.Println("  2) Your password changed since you last used Cago. Try again, using the '-p' flag to update your cached credentials.")
			cmd.Println("  3) Your password is expired or expiring. Try again after changing your password, using the '-p' flag to update your cached credentials.")

			return errors.WithMessage(samlAssertionErr, "unable to get SAML assertion")
		}

		log.Debugf("Retrieved Base64 encoded SAML assertion from IdP:\n%s", samlAssertionBase64)

		samlAssertion, decodeStringError := base64.StdEncoding.DecodeString(samlAssertionBase64)
		if decodeStringError != nil {
			return errors.WithMessage(decodeStringError, "Unable to decode SAML assertion")
		}

		log.Debugf("==================== SAML Assertion ====================\n%s\n==================== SAML Assertion ====================", string(samlAssertion))

		// Grab the list of authorized roles from the IdP
		authorizedRoles, rolesErr := saml.GetAuthorizedRoles(samlAssertionBase64)
		if rolesErr != nil {
			return errors.WithMessage(rolesErr, "Unable to get authorized roles")
		}

		log.Debugf("Found %d authorized roles in SAML assertion returned from IdP", len(authorizedRoles))

		if len(authorizedRoles) == 0 {
			cmd.Println("The identity provider says you are not authorized to assume any roles")
		}

		// First, let's make a map of roles to profile names that should exist based on the authorized role list
		authorizedProfileAndRoles := map[string]*aws.Role{}
		for _, awsRole := range authorizedRoles {
			// Translate the AWS role into a profile name and map it to the role
			profileName, translateRoleError := translateRoleToProfileName(awsRole)
			if translateRoleError != nil {
				return errors.Wrap(translateRoleError, "Unable to translate role to profile name")
			}

			authorizedProfileAndRoles[profileName] = awsRole
		}

		// Then, let's remove any profiles that are no longer authorized
		existingProfileNames, getAllManagedProfileNamesError := aws.GetAllManagedProfileNames()
		if getAllManagedProfileNamesError != nil {
			return errors.WithMessage(getAllManagedProfileNamesError, "Cago was unable to get the list of profile names")
		}

		for _, profileName := range existingProfileNames {
			if !funk.Contains(authorizedProfileAndRoles, profileName) {
				// Found a profile that is no longer authorized
				log.Infof("Removing profile (%s) as it no longer maps to an authorized role", profileName)

				deleteProfileError := aws.DeleteProfile(profileName)
				if deleteProfileError != nil {
					return errors.Wrapf(deleteProfileError, "Unable to delete profile: %s", profileName)
				}
			}
		}

		// Then, update each authorized profile
		for profileName, awsRole := range authorizedProfileAndRoles {
			log.Debugf("Processing profile: %v", profileName)

			// Check to see if the profile already exists
			profileExists, doesProfileExistError := aws.DoesProfileExist(profileName)
			if doesProfileExistError != nil {
				return errors.Wrapf(doesProfileExistError, "Cago cannot determine if the following profile exists: %s", profileName)
			}

			// Determine if the profile is expired
			profileIsExpired, isExpiredError := aws.IsExpired(profileName)
			if isExpiredError != nil {
				// Log this as debug, since normally the user doesn't care why the profile is expired
				log.Debugf("Cago cannot determine if the (%s) is expired: %+v", profileName, isExpiredError)
			}

			// Refresh the profile if it doesn't yet exist, it has expired or if the force refresh flag was set
			log.Debugf("Profile %v: Exists(%t) Expired(%t) ForceRefresh(%t)", profileName, profileExists, profileIsExpired, forceRefreshFlagValue)
			if forceRefreshFlagValue || !profileExists || profileIsExpired {
				if profileExists {
					// If we're going to refresh the profile, delete it first and create it fresh
					cmd.Printf("Refreshing profile: %s\n", profileName)

					deleteProfileError := aws.DeleteProfile(profileName)
					if deleteProfileError != nil {
						return errors.Wrapf(deleteProfileError, "Unable to delete profile: %s", profileName)
					}
				}

				log.Debugf("Assuming role (%s) with principal (%s)", awsRole.RoleARN, awsRole.PrincipalARN)

				roleCredentials, getTempCredentialsError := getTempCredentials(awsRole, &samlAssertionBase64)

				// Don't fail here, simply return the error if there is one
				if getTempCredentialsError != nil {
					return errors.Wrapf(getTempCredentialsError, "Error assuming role (%v) with principal (%v)", awsRole.RoleARN, awsRole.PrincipalARN)
				}

				if roleCredentials == nil {
					return errors.Errorf("No credentials found for role: %s", awsRole.RoleARN)
				}

				log.Debugf("Successfully assumed the role: %s", awsRole.RoleARN)

				// If the profile didn't exist before, tell the user
				if !profileExists {
					cmd.Printf("Creating new profile: %s\n", profileName)
				}

				addProfileError := aws.AddProfile(profileName)
				if addProfileError != nil {
					return errors.Wrapf(errors.WithStack(addProfileError), "Error creating new profile: %s", profileName)
				}

				// Store the role credentials in the profile section
				addAccessKeyValueError := aws.AddKeyValue(profileName, "aws_access_key_id", *roleCredentials.AccessKeyId)
				if addAccessKeyValueError != nil {
					return errors.Wrapf(addAccessKeyValueError, "Error adding new key (aws_access_key_id) to profile: %s", profileName)
				}

				addSecretKeyValueError := aws.AddKeyValue(profileName, "aws_secret_access_key", *roleCredentials.SecretAccessKey)
				if addSecretKeyValueError != nil {
					return errors.Wrapf(addSecretKeyValueError, "Error adding new key (aws_secret_access_key) to profile: %s", profileName)
				}

				addSessionKeyValueError := aws.AddKeyValue(profileName, "aws_session_token", *roleCredentials.SessionToken)
				if addSessionKeyValueError != nil {
					return errors.Wrapf(addSessionKeyValueError, "Error adding new key (aws_session_token) to profile: %s", profileName)
				}

				addExpirationKeyValueError := aws.AddKeyValue(profileName, aws.ExpirationKeyName, roleCredentials.Expiration.Format(time.RFC3339))
				if addExpirationKeyValueError != nil {
					return errors.Wrapf(addExpirationKeyValueError, "Error adding new key (%s) to profile: %s", aws.ExpirationKeyName, profileName)
				}

				addCagoKeyValueError := aws.AddKeyValue(profileName, aws.CagoManagedKeyName, "true")
				if addCagoKeyValueError != nil {
					return errors.Wrapf(addCagoKeyValueError, "Error adding new key (%s) to profile: %s", aws.CagoManagedKeyName, profileName)
				}

				// Add any additional key/value pairs from the configuration file to the profile
				addAdditionalProfileConfiguration(profileName, awsRole)
			} else {
				// The profile is valid and hasn't expired
				cmd.Printf("Skipping non-expired profile: %s\n", profileName)
			}
		}

		return nil
	},
}

// translateRoleToProfileName converts an array of SAML role names into the
func translateRoleToProfileName(awsRole *aws.Role) (string, error) {
	// Parse the ARN string
	roleARN, parseARNError := aws.ParseARN(awsRole.RoleARN)
	if parseARNError != nil {
		return "", errors.Wrapf(parseARNError, "Unable to parse ARN: %s", awsRole.RoleARN)
	}

	// Parse out to the role name, remove the 'role' part for clarity
	roleName := strings.TrimPrefix(roleARN.Resource, "role")

	var profileName string
	accountAlias := getAccountAlias(roleARN.Account)
	if accountAlias != "" {
		// Use the alias from the configuration file instead of the account id
		profileName = accountAlias + roleName

	} else {
		// The default profile name is the account id + rolename
		profileName = roleARN.Account + roleName
	}

	return profileName, nil
}

func getAccountAlias(accountID string) (accountAlias string) {
	// The Cago configuration file could have a section called 'Accounts.<Role Account>'
	accountSubTree := viper.Sub("Accounts." + accountID)
	if accountSubTree == nil {
		log.Debugf("No configuration found for account: %s", accountID)

		return ""
	}

	log.Debugf("Found configuration section for account under key: %s", "Accounts."+accountID)

	// Check to see if there's an alias
	accountAlias = accountSubTree.GetString("alias")
	if accountAlias == "" {
		log.Debugf("No 'alias' found for account: %s", accountID)

		return ""
	}

	return
}

func getAccountSTSRegion(accountID string) (stsRegion string) {
	// The Cago configuration file could have a section called 'Accounts.<Role Account>'
	accountSubTree := viper.Sub("Accounts." + accountID)
	if accountSubTree == nil {
		log.Debugf("No configuration found for account: %s", accountID)

		return ""
	}

	log.Debugf("Found configuration section for account under key: %s", "Accounts."+accountID)

	// Check to see if there's an alias
	stsRegion = accountSubTree.GetString("stsRegion")
	if stsRegion == "" {
		log.Debugf("No 'stsRegion' found for account: %s", accountID)

		return ""
	}

	return
}

// addAdditionalProfileConfiguration adds all key/value pairs from the account section of the configuration file to the profile
func addAdditionalProfileConfiguration(profileName string, awsRole *aws.Role) {
	// Parse the ARN string
	roleARN, parseARNError := aws.ParseARN(awsRole.RoleARN)
	if parseARNError != nil {
		log.Fatalf("Unable to parse ARN %s\n%+v", awsRole.RoleARN, errors.WithStack(parseARNError))
	}

	// Grab the configuration section for the account, if it exists
	accountSubTree := viper.Sub("Accounts." + roleARN.Account)

	if accountSubTree != nil {
		// Add any additional metadata from the config file
		for _, configurationKey := range accountSubTree.AllKeys() {
			configurationValue := accountSubTree.GetString(configurationKey)

			log.Debugf("Found additional configuration key/value pair: %s/%s", configurationKey, configurationValue)

			addAdditionalKeyValueError := aws.AddKeyValue(profileName, configurationKey, configurationValue)
			if addAdditionalKeyValueError != nil {
				log.Fatalf("Error creating new key in profile %s:\n%+v", profileName, errors.WithStack(addAdditionalKeyValueError))
			}
		}
	}
}

func getTempCredentials(awsRole *aws.Role, samlAssertionBase64 *string) (*sts.Credentials, error) {
	var stsService stsiface.STSAPI
	if !viper.GetBool("TESTMODE") {
		awsSession := session.Must(session.NewSession())

		// Parse the ARN string
		roleARN, parseARNError := aws.ParseARN(awsRole.RoleARN)
		if parseARNError != nil {
			return nil, errors.Wrapf(parseARNError, "Unable to parse ARN: %s", awsRole.RoleARN)
		}

		awsConfig := awssdk.NewConfig()

		// We might need to specify a region
		stsRegion := getAccountSTSRegion(roleARN.Account)
		if stsRegion != "" {
			log.Debugf("Using STS is region (%s) for role: %s", stsRegion, roleARN.String())

			awsConfig.Region = &stsRegion
		}

		stsService = sts.New(awsSession, awsConfig)
	} else {
		log.Debug("Using Mock STS!")

		stsService = aws.NewMockSTS()
	}

	assumeRoleWithSAMLInput := &sts.AssumeRoleWithSAMLInput{
		PrincipalArn:    &awsRole.PrincipalARN,
		RoleArn:         &awsRole.RoleARN,
		SAMLAssertion:   samlAssertionBase64,
		DurationSeconds: &sessionDurationFlagValue,
	}

	log.Debugf("Calling STS service")

	//  Don't fail here, simply return the error if there is one
	assumeRoleWithSAMLOutput, assumeRoleWithSAMLError := stsService.AssumeRoleWithSAML(assumeRoleWithSAMLInput)
	if assumeRoleWithSAMLError != nil {
		return nil, errors.Wrapf(assumeRoleWithSAMLError, "Call to AssumeRoleWithSAML failed with output:\n%+v", assumeRoleWithSAMLOutput)
	}

	if assumeRoleWithSAMLOutput == nil {
		return nil, errors.New("Call to AssumeRoleWithSAML returned no output")
	}

	// Pretty-print the response data.
	log.Debugf("STS Response:\n%v", assumeRoleWithSAMLOutput.String())

	return assumeRoleWithSAMLOutput.Credentials, assumeRoleWithSAMLError
}

func promptForUsername() (username string) {
	usernamePrompt := &survey.Input{
		Message: "Your SSO",
	}

	askOneError := survey.AskOne(usernamePrompt, &username, nil)
	if askOneError != nil {
		log.Errorf("Username prompt error: %+v", askOneError)
	}

	return username
}

func promptForPassword() (password string) {
	passwordPrompt := &survey.Password{
		Message: "Password",
	}

	askOneError := survey.AskOne(passwordPrompt, &password, nil)
	if askOneError != nil {
		log.Errorf("Password prompt error: %+v", askOneError)
	}

	return password
}
