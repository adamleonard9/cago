package cmd_test // import "electric-it.io/cago/cmd"

import (
	"bytes"
	"io/ioutil"
	"strings"
	"testing"
	"time"

	"github.com/aws/aws-sdk-go/service/sts"
	"github.com/pkg/errors"
	"github.com/spf13/viper"
	"github.com/zalando/go-keyring"

	"gopkg.in/jarcoal/httpmock.v1"

	"electric-it.io/cago/aws"
	"electric-it.io/cago/cmd"
	"electric-it.io/cago/cmd/credentials"
)

const (
	FAKESAMLENDPOINT = "https://fakesamlendpoint.local"
)

var (
	fakeAccessKeyID     = "FAKEAccessKeyID"
	fakeSessionToken    = "FAKESessionToken"
	fakeSecretAccessKey = "FAKESecretAccessKey"

	futureExpiration = time.Now().Add(time.Hour * 100)
)

func init() {
	viper.Set("TESTMODE", true)

	// Not testing input, so just use the cache
	keyring.MockInit()
	credentials.CacheUsername("FAKEUSERNAME")
	credentials.CachePassword("FAKEPASSWORD")

	viper.Set("AuthenticationUrl", FAKESAMLENDPOINT)
}

func TestConfigFileFlagFileNotExists(t *testing.T) {
	args := []string{"refresh-profiles", "-c", "this.file.does.not.exist.yml"}
	outputBuffer := bytes.NewBufferString("")

	executeError := cmd.ExecuteForTesting(args, outputBuffer)
	if executeError == nil {
		t.Errorf("Expected error when using config file flag without config file")
	}

	if outputBuffer.String() == "" {
		t.Error("Expected some output when running command with no arguments")
	}

	t.Logf("\n==================================  CLI Output  ==================================\n%s", outputBuffer.String())
}

func TestConfigFileFlagBadFile(t *testing.T) {
	args := []string{"refresh-profiles", "-c", "testdata/badConfigFile.yml"}
	outputBuffer := bytes.NewBufferString("")

	executeError := cmd.ExecuteForTesting(args, outputBuffer)
	if executeError == nil {
		t.Errorf("Expected error when using config file that is not YAML")
	}

	if outputBuffer.String() == "" {
		t.Error("Expected some output when running command with no arguments")
	}

	t.Logf("\n==================================  CLI Output  ==================================\n%s", outputBuffer.String())
}

func TestWithBadSTS_Sad_500Error(t *testing.T) {
	httpmock.Activate()
	defer httpmock.DeactivateAndReset()

	httpmock.RegisterResponder("HEAD", FAKESAMLENDPOINT, httpmock.NewStringResponder(200, ""))

	httpmock.RegisterResponder("POST", FAKESAMLENDPOINT, httpmock.NewStringResponder(500, ""))

	args := []string{"refresh-profiles", "-c", "testdata/goodConfigFile.yml"}
	outputBuffer := bytes.NewBufferString("")

	executeError := cmd.ExecuteForTesting(args, outputBuffer)
	if executeError == nil {
		t.Errorf("Expected an error when the STS doesn't respond to a POST with a 200")
	}

	if outputBuffer.String() == "" {
		t.Fatal("Expected some output when running command with no arguments")
	}

	t.Logf("\n==================================  CLI Output  ==================================\n%s", outputBuffer.String())
}

func TestWithMissingSAML(t *testing.T) {
	httpmock.Activate()
	defer httpmock.DeactivateAndReset()

	authenticationPostReturnFile, readFileError := ioutil.ReadFile("testdata/authenticationPostReturnMissingSAML.html")
	if readFileError != nil {
		t.Errorf("Unable to read in file: %s", readFileError)
	}

	httpmock.RegisterResponder("HEAD", FAKESAMLENDPOINT, httpmock.NewStringResponder(200, ""))

	httpmock.RegisterResponder("POST", FAKESAMLENDPOINT,
		httpmock.NewStringResponder(200, string(authenticationPostReturnFile)))

	args := []string{"refresh-profiles", "-c", "testdata/goodConfigFile.yml"}
	outputBuffer := bytes.NewBufferString("")

	executeError := cmd.ExecuteForTesting(args, outputBuffer)
	if executeError == nil {
		t.Errorf("Expected error when bad SAML returned from authentication")
	}

	if outputBuffer.String() == "" {
		t.Fatal("Expected some output when running command with no arguments")
	}

	t.Logf("\n==================================  CLI Output  ==================================\n%s", outputBuffer.String())
}

func TestWithBadSAML(t *testing.T) {
	httpmock.Activate()
	defer httpmock.DeactivateAndReset()

	authenticationPostReturnFile, readFileError := ioutil.ReadFile("testdata/authenticationPostReturnBadSAML.html")
	if readFileError != nil {
		t.Errorf("Unable to read in file: %s", readFileError)
	}

	httpmock.RegisterResponder("HEAD", FAKESAMLENDPOINT, httpmock.NewStringResponder(200, ""))

	httpmock.RegisterResponder("POST", FAKESAMLENDPOINT,
		httpmock.NewStringResponder(200, string(authenticationPostReturnFile)))

	args := []string{"refresh-profiles", "-c", "testdata/goodConfigFile.yml"}
	outputBuffer := bytes.NewBufferString("")

	executeError := cmd.ExecuteForTesting(args, outputBuffer)
	if executeError == nil {
		t.Errorf("Expected error when bad SAML returned from authentication")
	}

	t.Logf("\n==================================  CLI Output  ==================================\n%s", outputBuffer.String())
}

func TestWithNoRoles(t *testing.T) {
	httpmock.Activate()
	defer httpmock.DeactivateAndReset()

	authenticationPostReturnFile, readFileError := ioutil.ReadFile("testdata/authenticationPostReturnNoRoles.html")
	if readFileError != nil {
		t.Errorf("Unable to read in file: %s", readFileError)
	}

	httpmock.RegisterResponder("HEAD", FAKESAMLENDPOINT, httpmock.NewStringResponder(200, ""))

	httpmock.RegisterResponder("POST", FAKESAMLENDPOINT,
		httpmock.NewStringResponder(200, string(authenticationPostReturnFile)))

	args := []string{"refresh-profiles", "-c", "testdata/goodConfigFile.yml"}
	outputBuffer := bytes.NewBufferString("")

	executeError := cmd.ExecuteForTesting(args, outputBuffer)
	if executeError != nil {
		t.Fatalf("Error executing command: %+v", executeError)
	}

	t.Logf("\n==================================  CLI Output  ==================================\n%s", outputBuffer.String())
}

func TestCachedCredentialsHappy(t *testing.T) {
	httpmock.Activate()
	defer httpmock.DeactivateAndReset()

	authenticationPostReturnFile, readFileError := ioutil.ReadFile("testdata/authenticationPostReturn.html")
	if readFileError != nil {
		t.Errorf("Unable to read in file: %s", readFileError)
	}

	httpmock.RegisterResponder("HEAD", FAKESAMLENDPOINT, httpmock.NewStringResponder(200, ""))

	httpmock.RegisterResponder("POST", FAKESAMLENDPOINT,
		httpmock.NewStringResponder(200, string(authenticationPostReturnFile)))

	args := []string{"refresh-profiles", "-c", "testdata/goodConfigFile.yml"}
	outputBuffer := bytes.NewBufferString("")

	aws.MockAssumeRoleWithSAMLOutput = &sts.AssumeRoleWithSAMLOutput{
		Credentials: &sts.Credentials{
			AccessKeyId:     &fakeAccessKeyID,
			SessionToken:    &fakeSessionToken,
			SecretAccessKey: &fakeSecretAccessKey,
			Expiration:      &futureExpiration,
		},
	}
	aws.MockAssumeRoleWithSAMLError = nil

	executeError := cmd.ExecuteForTesting(args, outputBuffer)
	if executeError != nil {
		t.Fatalf("Error executing command: %+v", errors.WithStack(executeError))
	}

	if !strings.Contains(outputBuffer.String(), cmd.CACHED_USERNAME_MESSAGE) {
		t.Errorf("Expected output: " + cmd.CACHED_USERNAME_MESSAGE)
	}

	if !strings.Contains(outputBuffer.String(), cmd.CACHED_PASSWORD_MESSAGE) {
		t.Errorf("Expected output: " + cmd.CACHED_PASSWORD_MESSAGE)
	}

	// ==========================================================================
	// Run it again to ensure it skips the new profiles
	executeError = cmd.ExecuteForTesting(args, outputBuffer)
	if executeError != nil {
		t.Fatalf("Error executing command: %+v", errors.WithStack(executeError))
	}

	if !strings.Contains(outputBuffer.String(), "Skipping non-expired profile: alias1/role1") {
		t.Errorf("Expected output: Skipping non-expired profile: alias1/role1")
	}

	if !strings.Contains(outputBuffer.String(), "Skipping non-expired profile: alias1/role2") {
		t.Errorf("Expected output: Skipping non-expired profile: alias1/role2")
	}

	// ==========================================================================
	// Run it again with '-f' to ensure it force updates the new profiles
	args = []string{"refresh-profiles", "-f", "-c", "testdata/goodConfigFile.yml"}

	executeError = cmd.ExecuteForTesting(args, outputBuffer)
	if executeError != nil {
		t.Fatalf("Error executing command: %+v", errors.WithStack(executeError))
	}

	if !strings.Contains(outputBuffer.String(), "Skipping non-expired profile: alias1/role1") {
		t.Errorf("Expected output: Skipping non-expired profile: alias1/role1")
	}

	if !strings.Contains(outputBuffer.String(), "Skipping non-expired profile: alias1/role2") {
		t.Errorf("Expected output: Skipping non-expired profile: alias1/role2")
	}

	t.Logf("\n==================================  CLI Output  ==================================\n%s", outputBuffer.String())
}
