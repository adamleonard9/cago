[![pipeline status](https://gitlab.com/electric-it/cago/badges/master/pipeline.svg)](https://gitlab.com/electric-it/cago/commits/master)
[![codecov](https://codecov.io/gl/electric-it/cago/branch/master/graph/badge.svg)](https://codecov.io/gl/electric-it/cago)

# Cagophilist (aka Cago)
Cago (_pronounced kay-go_) is a tool for managing access to the AWS API from the command line. If you use command-line tools to manage AWS resources and you access AWS using temporary keys for SAML federated roles and have to frequently switch between different profiles as you work, Cago can help.

# What does Cago do?
In order to accessing the AWS API, a client must present credentials to authenticate their identity. These credentials are typically either semi-permanent API keys or temporary tokens which are granted to allow access to assume a particular IAM role. Cago was built to help with the latter case.

Cago's job is to maintain a set of profiles in the AWS credentials file (i.e. `~/.aws/credentials`). These profiles contain temporary keys, which allow the client (either the AWS CLI or another CLI using the AWS SDK) to access the AWS API.

## Typical Cago refresh
1. Cago will ask for your single sign on (SSO) credentials.
2. Cago will authenticate you against a SAML service provider, following redirects until it receives a SAML assertion.
3. Cago will parse the SAML assertion to extract the AWS roles that you are authorized to assume.
4. For each role...
    - Cago will request a set of temporary tokens from the AWS Security Token Service (STS)
    - Cago will create or update a profile in the AWS credentials file

Because these tokens are temporary and expire, you'll end up running Cago periodically to refresh the profiles.

Note: Cago adds a special key `cago_managed=true` to each key in your credentials file. This will allow you to maintain other profiles that Cago will not touch.

## Setting the active profile
At runtime, the AWS CLI and SDK read an environment variables named `AWS_PROFILE, AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY, AWS_SESSION_TOKEN` to determine the credentials to use when calling the AWS API.

Cago allows you to easily set and update these environment variables.

# Cago components
## Executable
Cago is mainly a compiled executable written in Go. It is released in MacOS, Windows and Linux flavors.

## Wrapper script
Because Cago interacts with environment variables local to your command-line session, a shell script is required as the executable cannot mess with environment variables directly. A POSIX compliant shell script and a Windows batch file are provided.

## Configuration file
Cago is highly customizable, allowing you to tweak and tune it for your specific environment. These configurations are held in a YAML file that can be stored locally or remotely. By storing the file remotely, many Cago users in a single enterprise can get a working configuration with little effort.

## Alias commands
These are a final layer to simplify the use of Cago. The allow you to run all of the common Cago functions using a few simple commands: `cagor`, `cagos`, `cagou`, `cagol`

# Installing Cago on a Mac
## Homebrew
`brew tap electric-it/electric-it`

`brew install cago`

## Manual
Download the latest release and unzip the files to your path. You can then run `cago init` to setup Cago.

## Alias commands
It's recommended that you can create aliases by adding the following to your login scripts (e.g. `~/.bashrc`):
```
alias cagol='source /usr/local/bin/cago.sh list'
alias cagor='source /usr/local/bin/cago.sh refresh'
alias cagos='source /usr/local/bin/cago.sh switch'
alias cagou='source /usr/local/bin/cago.sh unset'
```

# Installing Cago on Windows
## Manual
Download the latest release and unzip the files to a folder on your path. You can then run `cago init` to setup Cago.

## Remote config file
Cago can pull a configuration file from a remote URL, so you can pickup changes from a common place. The config file will be cached on disk for when the remote URL is not accessible.
```
REM First set the environment variable that points to the remote config file
set CAGO_CONFIG_URL=TBD
setx CAGO_CONFIG_URL %CAGO_CONFIG_URL%
```

## Alias commands
On Windows, you can set a current environment variable from a command. You can use `doskey` to create aliases just like in \*nix:
```
REM The define the shortcuts to run Cago
doskey cagor=cago_win refresh-profiles
doskey cagol=cago_win list-profiles
doskey cagos=cago_win switch-profile
doskey cagou=cago_win unset
```

# Cago Commands
## Refresh (cagor)
What it does:
1) Checks each Cago managed profile in your AWS credentials file, if none of them have expired, Cago exits. Note: You can override this behavior by adding the `-f` or `--force-refresh` flag.
2) Cago asks for your credentials so that it can authenticate you and find out what roles you can use. Note: Cago will cache your credentials in your operating system secure store so you don't have to enter them again. Use the `p` or `prompt-for-credentials` flag to force Cago to ask for your credentials again.
3) Cago authenticates against the IdP and downloads a SAML assertion that lists all of the roles you can assume in AWS.
4) Cago creates or updates a profile in your AWS credentials file for each of those roles.
5) Cago cleans up your AWS credentials file by removing any Cago managed profiles that don't match a role in the SAML assertion.
6) Cago talks to the AWS token service to retrieve a fresh token for each profile in your AWS credentials file

_Note: Be sure to run `cagor` once you get setup for the first time. This will sync your profiles to your authorized roles!_

## Switch (cagos)
This command allows the user to switch to using a different profile. It allows the user to select a role and then updates the appropriate environment variables used by the CLI and SDK.

The flag `--set-aws-profile-only` will tell Cago to only set the  AWS_PROFILE environment variables. Cago will unset AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY and AWS_SESSION_TOKEN. (Note: Currently not implemented for Windows)

## List (cagol)
This command will list all the profiles in the credentials file that Cago is managing.

## Unset (cagou)
Unsets all of the Cago managed AWS environment variables.

# Proxy Magic
We all love having to use proxy servers to reach the internet... right?! To try to make life a bit simpler, Cago tries to do some magic when proxies are present.

Here are the steps Cago takes to try to make your proxy-bound life easier...
1. If the `HTTP_PROXY` or `http_proxy` environment variables are set, you probably know what you're doing, Cago will use that value.
2. If the `--ignore-proxy-config` flag is set, you are telling Cago to not bother with any proxy configurations in the _configuration file_. Note, this flag is useless if any of the above environment variables are set.
3. Here be the magic! Cago will now go through each of the proxies in the HTTPProxies section of the configuration file. If Cago finds a working proxy, it'll use it! What a smarty! If none of them work, Cago will not use a proxy.

Hopefully this helps?

# Configuration File
Cago needs to read in a YAML file containing configuration information in order to run properly.

In order of highest precedence, Cago will read configuration information from:
1) Local file specified by `config-file` argument
    - If you pass the name of a config file using the `-c` or `--config-file` argument, Cago will use that
2) Remote URL
    - If you do _not_ pass the `-c` argument, but you _do_ set the `CAGO_CONFIG_URL` environment variable , Cago will download the configuration file from this HTTP or HTTPS URL
    - If successfully downloaded, the file will be saved to `.cago/cached.cago.yaml`
3) Locally cache remote file
    - If the `CAGO_CONFIG_URL` environment variable _is_ set, but Cago is unable to download the configuration file, it will use the previously cached configuration file from `.cago/cached.cago.yaml`
4) Local file in your home directory
    - If the `CAGO_CONFIG_URL` environment variable is _not_ set and the `-c` argument is _not_ passed, Cago will look for a file named `.cago/cago.yaml`

## Configuration File Example
```
# This is the default authentication URL for retrieving the SAML assertion
AuthenticationURL: <URL>

# This is the default target URLs for all accounts not listed in the AlternativeAuthenticationURLs section
TargetURL: <URL>

# List of all HTTP proxies to try to use, in order they'll be tried
# The first proxy on the list will be used for HTTP connections, unless the '--ignore-proxy' flag is enabled
# Format is...
# HTTPProxies - List of all proxies
#   ProxyAlias1: - Human readable alias for the proxy
#     ProxyURL: <What you would normally use the HTTP_PROXY environment variable for>
#     NoProxy: <What you would normally use the NO_PROXY environment variable for>
HTTPProxies:
  GENetwork:
    ProxyURL: "http://proxy.mycompany.com:80"
    NoProxy:  "localhost,.mycompany.com"

# Account metadata that Cago will add to the AWS profile file when it runs
AccountProfiles:
  <Account #>:
    alias:       <This is a keyword that tells Cago to use this string instead of the account id for profile names>
    stsRegion:   <This is a keyword that tells Cago to use the STS endpoint in a specific region. A must have for GovCloud roles!>
    anything:    <This is a random key value pair that will be added to the profile file. (Setting region is very handy!)>

  867530933333939:
    alias:       acct2
    stsRegion:   us-east-1
    region:      us-east-1
    Description: You can even add a nice description that will be copied in to the profiles

  106101110110121:
    alias:       org1east
    region:      us-gov-west-1
    stsRegion:   us-gov-east-1
```

### AuthenticationURL
This is the URL that your username/password will be posted.

### TargetURL
This is where Cago will retrieve the SAML assertion from.

### AccountAlias
Maps account numbers to human readable names.

### Overriding the configuration file
You can override any of these at runtime by specifying an environment variable like `CAGO_TARGETURL=https://my.other.target`. The variable must be uppercase and prefixed with CAGO_

# Troubleshooting
## Unable to locate SAMLResponse
SAML is meant to be used by a human with a browser. It was never really intended to be used by a CLI like Cago. Unfortunately, in many cases, we're stuck with SAML and need to work with it. One of the main drawbacks is that SAML talks HTML. This means that when something goes wrong, Cago is presented with an HTML page meant to be displayed to a human. This makes handling errors somewhat impossible as it's often not possible to scrape anything useful from the HTML to understand what went wrong.

Cago expects the HTML that gets returned after authentication to contain a SAML assertion in specific place. When something goes wrong, Cago can only say `Unable to locate SAMLResponse` because Cago doesn't have a way to tell exactly what went wrong.

Typically, the `Unable to locate SAMLResponse` error means that you may have:
1. Entered your credentials incorrectly, run `cagor` again with the `-p` flag to try again.
1. Entered your credentials _correctly_, but your password is expired, run `cagor` again with the `-p` flag to try again.
1. Entered your credentials _correctly_, but your password is _expiring_, go change your password then run `cagor` again with the `-p` flag to try again. (This happens because the HTML page that gets returned is trying to tell you to go change your password.)