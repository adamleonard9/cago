package saml // import "electric-it.io/cago/saml"

import (
	"encoding/base64"
	"fmt"
	"net/http"
	"net/http/cookiejar"
	"net/url"
	"strings"

	"github.com/PuerkitoBio/goquery"
	"github.com/apex/log"
	"github.com/beevik/etree"
	"github.com/pkg/errors"
	"github.com/spf13/viper"
	"github.com/yosssi/gohtml"

	"golang.org/x/net/publicsuffix"

	"electric-it.io/cago/aws"
)

const (
	SAML_ROLES_ELEMENT_NAME = "https://aws.amazon.com/SAML/Attributes/Role"
)

// GetSAMLAssertionBase64 Asks the user for their credentials and then retrieves the SAML assertion from the IdP
func GetSAMLAssertionBase64(username string, password string) (string, error) {
	options := &cookiejar.Options{
		PublicSuffixList: publicsuffix.List,
	}

	jar, newCookieJarError := cookiejar.New(options)
	if newCookieJarError != nil {
		log.Fatalf("Error creating new cookie jar: %s", newCookieJarError)
	}

	client := &http.Client{
		// Some fancy trickery to figure out if authentication failed
		CheckRedirect: checkRedirect,
		Jar:           jar,
	}

	form := url.Values{}
	form.Add("username", username)
	form.Add("password", password)

	targetURL := viper.GetString("TargetUrl")
	form.Add("TARGET", targetURL)

	authenticationURL := viper.GetString("AuthenticationUrl")
	if authenticationURL == "" {
		return "", errors.New("The AuthenticationUrl is not set")
	}

	log.Debugf("Attempting POST to the IdP: %s with a target %s", authenticationURL, targetURL)

	response, postError := client.Post(authenticationURL, "application/x-www-form-urlencoded", strings.NewReader(form.Encode()))
	if postError != nil {
		return "", errors.Wrap(postError, "Error posting to authentication URL")
	}

	log.Debugf("IdP POST return status: %d", response.StatusCode)

	if response.StatusCode != http.StatusOK {
		return "", errors.Errorf("Bad response status returned from authentication URL: %d", response.StatusCode)
	}

	responseBody := response.Body
	if responseBody == nil {
		return "", errors.New("Response from IdP was empty")
	}

	doc, newDocumentFromReaderError := goquery.NewDocumentFromReader(response.Body)
	if newDocumentFromReaderError != nil {
		log.Errorf("Error creating new document from response")

		return "", errors.WithMessage(newDocumentFromReaderError, "Error creating new document from response")
	}

	samlAssertionBase64, exists := doc.Find("input[name='SAMLResponse']").Attr("value")
	if !exists {
		responseHTML, _ := doc.Html()
		log.Debug(gohtml.Format(responseHTML))

		return "", errors.New("Unable to locate SAMLResponse")
	}

	return samlAssertionBase64, nil
}

// GetAuthorizedRoles Returns a list of all roles the user is authorized to assume
func GetAuthorizedRoles(samlAssertionBase64 string) ([]*aws.Role, error) {
	samlAssertion, decodeStringError := base64.StdEncoding.DecodeString(samlAssertionBase64)
	if decodeStringError != nil {
		log.Errorf("Error decoding SAML assertion: %s", decodeStringError)
		return nil, decodeStringError
	}

	extractedRoles, extractAwsRolesError := ExtractAwsRoles(samlAssertion)
	if extractAwsRolesError != nil {
		log.Errorf("Error extracting AWS roles: %s", extractAwsRolesError)
		return nil, extractAwsRolesError
	}

	authorizedRoles, parseAWSRolesError := aws.ParseAWSRoles(extractedRoles)
	if parseAWSRolesError != nil {
		log.Errorf("Error parsing AWS roles: %s", parseAWSRolesError)
		return nil, parseAWSRolesError
	}

	return authorizedRoles, nil
}

// This method will get called during a redirect, tries to guess when authentication fails
func checkRedirect(req *http.Request, via []*http.Request) error {
	hostname := req.URL.Hostname()
	log.Debugf("Redrecting to %s", hostname)

	return nil
}

// Original Source: https://github.com/Versent/saml2aws
// Original License:
//   This code is Copyright (c) 2015 Versent and released under the MIT license.
//   All rights not explicitly granted in the MIT license are reserved.
//
const (
	assertionTag          = "Assertion"
	attributeStatementTag = "AttributeStatement"
	attributeTag          = "Attribute"
	attributeValueTag     = "AttributeValue"
)

//ErrMissingElement is the error type that indicates an element and/or attribute is
//missing. It provides a structured error that can be more appropriately acted
//upon.
type ErrMissingElement struct {
	Tag, Attribute string
}

//ErrMissingAssertion indicates that an appropriate assertion element could not
//be found in the SAML Response
var (
	ErrMissingAssertion = ErrMissingElement{Tag: assertionTag}
)

func (e ErrMissingElement) Error() string {
	if e.Attribute != "" {
		return fmt.Sprintf("missing %s attribute on %s element", e.Attribute, e.Tag)
	}

	return fmt.Sprintf("missing %s element", e.Tag)
}

// ExtractAwsRoles given an assertion document extract the aws roles
func ExtractAwsRoles(data []byte) ([]string, error) {
	awsRoles := []string{}

	doc := etree.NewDocument()
	if err := doc.ReadFromBytes(data); err != nil {
		return awsRoles, err
	}

	assertionElement := doc.FindElement(".//Assertion")
	if assertionElement == nil {
		return nil, ErrMissingAssertion
	}

	// Get the actual assertion attributes
	attributeStatement := assertionElement.FindElement(childPath(assertionElement.Space, attributeStatementTag))
	if attributeStatement == nil {
		return nil, ErrMissingElement{Tag: attributeStatementTag}
	}

	attributes := attributeStatement.FindElements(childPath(assertionElement.Space, attributeTag))
	for _, attribute := range attributes {
		if attribute.SelectAttrValue("Name", "") != SAML_ROLES_ELEMENT_NAME {
			continue
		}

		attributeValues := attribute.FindElements(childPath(assertionElement.Space, attributeValueTag))
		for _, attrValue := range attributeValues {
			awsRoles = append(awsRoles, attrValue.Text())
		}
	}

	return awsRoles, nil
}

func childPath(space, tag string) string {
	if space == "" {
		return "./" + tag
	}

	return "./" + space + ":" + tag
}
